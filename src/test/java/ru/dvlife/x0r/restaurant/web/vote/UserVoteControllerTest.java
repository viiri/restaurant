package ru.dvlife.x0r.restaurant.web.vote;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.dvlife.x0r.restaurant.to.RestaurantTo;
import ru.dvlife.x0r.restaurant.to.VoteTo;
import ru.dvlife.x0r.restaurant.util.JsonUtil;
import ru.dvlife.x0r.restaurant.web.restaurant.RestaurantTestData;
import ru.dvlife.x0r.restaurant.model.Vote;
import ru.dvlife.x0r.restaurant.repository.VoteRepository;
import ru.dvlife.x0r.restaurant.web.AbstractControllerTest;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.dvlife.x0r.restaurant.web.user.UserTestData.GUEST_MAIL;
import static ru.dvlife.x0r.restaurant.web.user.UserTestData.USER_MAIL;
import static ru.dvlife.x0r.restaurant.web.vote.UserVoteController.REST_URL;
import static ru.dvlife.x0r.restaurant.web.vote.VoteTestData.*;

public class UserVoteControllerTest extends AbstractControllerTest {
    private static final String REST_URL_SLASH = REST_URL + '/';

    @Autowired
    private Clock clock;

    @Autowired
    private VoteRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    void getUnAuth() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL_SLASH + "current"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails(value = USER_MAIL)
    void get() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL_SLASH + "current"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(VOTE_TO_MATCHER.contentJson(VoteTestData.voteTo3));
    }

    @Test
    @WithUserDetails(value = GUEST_MAIL)
    void getNotFound() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL_SLASH + "current"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(value = GUEST_MAIL)
    void createWithLocation() throws Exception {
        ResultActions actions = perform(MockMvcRequestBuilders.post(UserVoteController.REST_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(new RestaurantTo(RestaurantTestData.RESTAURANT_ID + 2))))
                .andExpect(status().isCreated());

        VoteTo created = VOTE_TO_MATCHER.readFromJson(actions);

        int newId = created.id();
        Vote newVote = getNew(clock);
        newVote.setId(newId);
        VoteTo newVoteTo = modelMapper.map(newVote, VoteTo.class);

        VOTE_TO_MATCHER.assertMatch(created, newVoteTo);
        VOTE_MATCHER.assertMatch(repository.getExisted(newId), newVote);
    }

    @Test
    @WithUserDetails(value = USER_MAIL)
    void update() throws Exception {
        perform(MockMvcRequestBuilders.put(REST_URL_SLASH + (VOTE_ID + 2))
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(new RestaurantTo(RestaurantTestData.RESTAURANT_ID + 2))))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @TestConfiguration
    static class CustomClockConfiguration {
        private static final LocalDateTime fixedDate = LocalDateTime.of(2020, Month.JANUARY, 31, 10, 0, 0);

        @Bean
        @Primary
        public Clock fixedClock() {
            return Clock.fixed(fixedDate.toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
        }
    }
}
