package ru.dvlife.x0r.restaurant.web.menu;

import ru.dvlife.x0r.restaurant.model.Dish;
import ru.dvlife.x0r.restaurant.to.MenuTo;
import ru.dvlife.x0r.restaurant.web.restaurant.RestaurantTestData;
import ru.dvlife.x0r.restaurant.model.Menu;
import ru.dvlife.x0r.restaurant.web.MatcherFactory;

import java.time.LocalDate;
import java.util.List;

public class MenuTestData {
    public static final MatcherFactory.Matcher<Menu> MENU_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(Menu.class, "restaurant");
    public static final MatcherFactory.Matcher<MenuTo> MENU_TO_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(MenuTo.class);

    public static final int MENU_ID = 1;
    public static final int DISH_ID = 1;
    public static final int NOT_FOUND = 100;

    public static final Dish dish1 = new Dish(DISH_ID, "Restaurant 1, Dish #1", 3);
    public static final Dish dish2 = new Dish(DISH_ID + 1, "Restaurant 1, Dish #2", 2);
    public static final Dish dish3 = new Dish(DISH_ID + 2, "Restaurant 1, Dish #3", 1);
    public static final Dish dish4 = new Dish(DISH_ID + 3, "Test Restaurant 2, Dish #1", 2);
    public static final Dish dish5 = new Dish(DISH_ID + 4, "Test Restaurant 2, Dish #2", 1);
    public static final Dish dish6 = new Dish(DISH_ID + 5, "Test Restaurant 2, Dish #3", 4);
    public static final Dish dish7 = new Dish(DISH_ID + 6, "Test Restaurant 2, Dish #4", 3);

    public static final Dish dishTo1 = new Dish(null,"Restaurant 1, Dish #1", 3);
    public static final Dish dishTo2 = new Dish(null, "Restaurant 1, Dish #2", 2);
    public static final Dish dishTo3 = new Dish(null, "Restaurant 1, Dish #3", 1);
    public static final Dish dishTo4 = new Dish(null, "Test Restaurant 2, Dish #1", 2);
    public static final Dish dishTo5 = new Dish(null, "Test Restaurant 2, Dish #2", 1);
    public static final Dish dishTo6 = new Dish(null, "Test Restaurant 2, Dish #3", 4);
    public static final Dish dishTo7 = new Dish(null, "Test Restaurant 2, Dish #4", 3);

    public static final Menu menu1 = new Menu(MENU_ID, RestaurantTestData.restaurant1, LocalDate.parse("2020-01-30"), List.of(dish1, dish2, dish3));
    public static final Menu menu2 = new Menu(MENU_ID + 1, RestaurantTestData.restaurant2, LocalDate.parse("2020-01-30"), List.of(dish4, dish5));
    public static final Menu menu3 = new Menu(MENU_ID + 2, RestaurantTestData.restaurant2, LocalDate.parse("2020-01-31"), List.of(dish6, dish7));

    public static final Menu menu1_dishTo = new Menu(MENU_ID, RestaurantTestData.restaurant1, LocalDate.parse("2020-01-30"), List.of(dishTo1, dishTo2, dishTo3));
    public static final Menu menu2_dishTo = new Menu(MENU_ID + 1, RestaurantTestData.restaurant2, LocalDate.parse("2020-01-30"), List.of(dishTo4, dishTo5));

    public static final MenuTo menuTo1 = new MenuTo(MENU_ID, RestaurantTestData.restaurant1.id(), LocalDate.parse("2020-01-30"), List.of(dishTo1, dishTo2, dishTo3));
    public static final MenuTo menuTo2 = new MenuTo(MENU_ID + 1, RestaurantTestData.restaurant2.id(), LocalDate.parse("2020-01-30"), List.of(dishTo4, dishTo5));
    public static final MenuTo menuTo3 = new MenuTo(MENU_ID + 2, RestaurantTestData.restaurant2.id(), LocalDate.parse("2020-01-31"), List.of(dishTo6, dishTo7));

    public static final List<MenuTo> menuTos = List.of(menuTo3, menuTo1, menuTo2);

    public static MenuTo getNew() {
        return new MenuTo(null, RestaurantTestData.restaurant3.id(), LocalDate.now(), List.of(
                new Dish(null, "New Dish #1", 11),
                new Dish(null, "New Dish #2", 22)
        ));
    }

    public static Menu getUpdated() {
        Menu menu = new Menu(menu1);

        menu.setDishes(
                List.of(
                        new Dish(null, "Updated Dish", 333)
                )
        );

        return menu;
    }
}
