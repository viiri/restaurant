package ru.dvlife.x0r.restaurant.web.menu;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.dvlife.x0r.restaurant.to.MenuTo;
import ru.dvlife.x0r.restaurant.util.JsonUtil;
import ru.dvlife.x0r.restaurant.web.restaurant.RestaurantTestData;
import ru.dvlife.x0r.restaurant.model.Menu;
import ru.dvlife.x0r.restaurant.repository.MenuRepository;
import ru.dvlife.x0r.restaurant.web.AbstractControllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.dvlife.x0r.restaurant.web.menu.AdminMenuController.REST_URL;
import static ru.dvlife.x0r.restaurant.web.menu.MenuTestData.*;
import static ru.dvlife.x0r.restaurant.web.user.UserTestData.ADMIN_MAIL;
import static ru.dvlife.x0r.restaurant.web.user.UserTestData.USER_MAIL;

public class AdminMenuControllerTest extends AbstractControllerTest {
    private static final String REST_URL_SLASH = REST_URL + '/';

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MenuRepository repository;

    @Test
    void getUnAuth() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails(value = USER_MAIL)
    void getForbidden() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void get() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL_SLASH + MENU_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MENU_TO_MATCHER.contentJson(menuTo1));
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void getNotFound() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL_SLASH + NOT_FOUND))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void getByRestaurantId() throws Exception {
        perform(MockMvcRequestBuilders.get(REST_URL_SLASH + "by-restaurant")
                .param("id", Integer.toString(RestaurantTestData.RESTAURANT_ID + 1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MENU_TO_MATCHER.contentJson(menuTo3, menuTo2));
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void delete() throws Exception {
        perform(MockMvcRequestBuilders.delete(REST_URL_SLASH + MENU_ID))
                .andDo(print())
                .andExpect(status().isNoContent());
        assertFalse(repository.findById(MENU_ID).isPresent());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void deleteNotFound() throws Exception {
        perform(MockMvcRequestBuilders.delete(REST_URL_SLASH + NOT_FOUND))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void createWithLocation() throws Exception {
        MenuTo newMenu = getNew();
        ResultActions actions = perform(MockMvcRequestBuilders.post(REST_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(newMenu)))
                .andExpect(status().isCreated());

        MenuTo created = MENU_TO_MATCHER.readFromJson(actions);
        int newId = created.id();
        MenuTo menuTo = modelMapper.map(repository.getExisted(newId), MenuTo.class);

        assertEquals(menuTo.getRestaurantId(), created.getRestaurantId());
        assertEquals(menuTo.getDate(), created.getDate());
        assertEquals(menuTo.getDishes().get(0).getName(), created.getDishes().get(0).getName());
        assertEquals(menuTo.getDishes().get(0).getPrice(), created.getDishes().get(0).getPrice());
        assertEquals(menuTo.getDishes().get(1).getName(), created.getDishes().get(1).getName());
        assertEquals(menuTo.getDishes().get(1).getPrice(), created.getDishes().get(1).getPrice());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void createInvalid() throws Exception {
        Menu invalid = new Menu(null, null, null, null);
        perform(MockMvcRequestBuilders.post(REST_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(invalid)))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void update() throws Exception {
        MenuTo updateTo = modelMapper.map(getUpdated(), MenuTo.class);
        updateTo.setId(null);
        perform(MockMvcRequestBuilders.put(REST_URL_SLASH + MENU_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(updateTo)))
                .andDo(print())
                .andExpect(status().isNoContent());

        Menu updated = repository.getExisted(MENU_ID);
        assertEquals(updated.getDishes().get(0).getName(), getUpdated().getDishes().get(0).getName());
        assertEquals(updated.getDishes().get(0).getPrice(), getUpdated().getDishes().get(0).getPrice());
    }

    @Test
    @WithUserDetails(value = ADMIN_MAIL)
    void updateInvalid() throws Exception {
        MenuTo invalid = new MenuTo(null, null, null, null);

        perform(MockMvcRequestBuilders.put(REST_URL_SLASH + MENU_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(invalid)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }
}
