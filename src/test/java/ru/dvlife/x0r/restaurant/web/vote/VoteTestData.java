package ru.dvlife.x0r.restaurant.web.vote;

import org.springframework.stereotype.Component;
import ru.dvlife.x0r.restaurant.to.VoteTo;
import ru.dvlife.x0r.restaurant.web.restaurant.RestaurantTestData;
import ru.dvlife.x0r.restaurant.model.Vote;
import ru.dvlife.x0r.restaurant.web.MatcherFactory;

import java.time.Clock;
import java.time.LocalDate;

import static ru.dvlife.x0r.restaurant.web.user.UserTestData.guest;
import static ru.dvlife.x0r.restaurant.web.user.UserTestData.user;

@Component
public class VoteTestData {
    public static final MatcherFactory.Matcher<Vote> VOTE_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(Vote.class, "user", "restaurant");
    public static final MatcherFactory.Matcher<VoteTo> VOTE_TO_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(VoteTo.class);

    public static final int VOTE_ID = 1;

    public static final VoteTo voteTo3 = new VoteTo(VOTE_ID + 2, user.id(), RestaurantTestData.restaurant2.id(), LocalDate.parse("2020-01-31"));

    public static Vote getNew(Clock clock) {
        return new Vote(null, guest, RestaurantTestData.restaurant3, LocalDate.now(clock));
    }
}
