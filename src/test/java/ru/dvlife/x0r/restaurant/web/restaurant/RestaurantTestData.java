package ru.dvlife.x0r.restaurant.web.restaurant;

import ru.dvlife.x0r.restaurant.model.Restaurant;
import ru.dvlife.x0r.restaurant.web.MatcherFactory;

import java.util.List;

public class RestaurantTestData {
    public static final MatcherFactory.Matcher<Restaurant> RESTAURANT_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(Restaurant.class);

    public static final int RESTAURANT_ID = 1;
    public static final int NOT_FOUND = 100;

    public static final Restaurant restaurant1 = new Restaurant(RESTAURANT_ID,"Restaurant 1");
    public static final Restaurant restaurant2 = new Restaurant(RESTAURANT_ID + 1,"Test Restaurant 2");
    public static final Restaurant restaurant3 = new Restaurant(RESTAURANT_ID + 2,"Test Restaurant 3");

    public static final List<Restaurant> restaurants = List.of(restaurant1, restaurant2, restaurant3);

    public static Restaurant getNew() {
        return new Restaurant(null, "New Restaurant");
    }

    public static Restaurant getUpdated() {
        return new Restaurant(RESTAURANT_ID, "UpdatedName");
    }
}
