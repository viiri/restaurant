package ru.dvlife.x0r.restaurant.web.vote;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.dvlife.x0r.restaurant.to.RestaurantTo;
import ru.dvlife.x0r.restaurant.util.JsonUtil;
import ru.dvlife.x0r.restaurant.web.AbstractControllerTest;
import ru.dvlife.x0r.restaurant.web.restaurant.RestaurantTestData;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.dvlife.x0r.restaurant.web.user.UserTestData.USER_MAIL;
import static ru.dvlife.x0r.restaurant.web.vote.UserVoteController.REST_URL;
import static ru.dvlife.x0r.restaurant.web.vote.VoteTestData.VOTE_ID;

public class UserVoteControllerTimeLimitTest extends AbstractControllerTest {
    private static final String REST_URL_SLASH = REST_URL + '/';

    @Test
    @WithUserDetails(value = USER_MAIL)
    void update() throws Exception {
        perform(MockMvcRequestBuilders.put(REST_URL_SLASH + (VOTE_ID + 2))
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.writeValue(new RestaurantTo(RestaurantTestData.RESTAURANT_ID + 2))))
                .andDo(print())
                .andExpect(status().isConflict());
    }


    @TestConfiguration
    static class CustomClockConfiguration {
        private static final LocalDateTime fixedDateTime = LocalDateTime.of(2020, Month.JANUARY, 31, 12, 0, 0);

        @Bean
        @Primary
        public Clock fixedClock() {
            return Clock.fixed(fixedDateTime.toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
        }
    }
}
