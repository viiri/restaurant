INSERT INTO USERS (name, email, password)
VALUES ('User', 'user@yandex.ru', '{noop}password'),
       ('Admin', 'admin@gmail.com', '{noop}admin'),
       ('Guest', 'guest@gmail.com', '{noop}guest');

INSERT INTO USER_ROLE (role, user_id)
VALUES ('USER', 1),
       ('ADMIN', 2),
       ('USER', 2);

INSERT INTO RESTAURANTS (name)
VALUES ('Restaurant 1'),
       ('Test Restaurant 2'),
       ('Test Restaurant 3');

INSERT INTO MENUS (date, restaurant_id)
VALUES ('2020-01-30', 1),
       ('2020-01-30', 2),
       ('2020-01-31', 2);

INSERT INTO DISHES (name, price, menu_id)
VALUES ('Restaurant 1, Dish #1', 3, 1),
       ('Restaurant 1, Dish #2', 2, 1),
       ('Restaurant 1, Dish #3', 1, 1),
       ('Test Restaurant 2, Dish #1', 2, 2),
       ('Test Restaurant 2, Dish #2', 1, 2),
       ('Test Restaurant 2, Dish #3', 4, 3),
       ('Test Restaurant 2, Dish #4', 3, 3);

INSERT INTO VOTES (user_id, restaurant_id, created)
VALUES (1, 1, '2020-01-30'),
       (3, 1, '2020-01-30'),
       (1, 2, '2020-01-31');
