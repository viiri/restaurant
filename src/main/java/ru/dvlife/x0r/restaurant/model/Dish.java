package ru.dvlife.x0r.restaurant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "dishes")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Dish extends NamedEntity {
    @Column(name = "price", nullable = false)
    @NotNull
    @Range(min = 1)
    private Integer price;

    public Dish(Integer id, String name, Integer price) {
        super(id, name);
        this.price = price;
    }

    @Override
    @JsonIgnore
    public Integer getId() {
        return super.getId();
    }
}
