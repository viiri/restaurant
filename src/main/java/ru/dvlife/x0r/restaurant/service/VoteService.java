package ru.dvlife.x0r.restaurant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dvlife.x0r.restaurant.error.DataConflictException;
import ru.dvlife.x0r.restaurant.error.NotFoundException;
import ru.dvlife.x0r.restaurant.model.Vote;
import ru.dvlife.x0r.restaurant.repository.RestaurantRepository;
import ru.dvlife.x0r.restaurant.repository.UserRepository;
import ru.dvlife.x0r.restaurant.repository.VoteRepository;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;

@Service
public class VoteService {
    private static final LocalTime timeLimit = LocalTime.of(11, 0);

    @Autowired
    private Clock clock;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private UserRepository userRepository;

    public Vote getCurrentByUser(int userId) {
        return voteRepository.findByUserIdAndCreated(userId, LocalDate.now(clock)).orElseThrow(() -> new NotFoundException("Vote with user_id=" + userId + " not found"));
    }

    @Transactional
    public Vote create(int restaurantId, int userId) {
        Vote vote = new Vote(userRepository.getReferenceById(userId),
                restaurantRepository.getReferenceById(restaurantId),
                LocalDate.now(clock));

        return voteRepository.save(vote);
    }

    @Transactional
    public void update(int id, int restaurantId, int userId) {
        LocalDate date = LocalDate.now(clock);
        LocalTime time = LocalTime.now(clock);

        Vote vote = voteRepository.findByIdAndUserIdAndCreated(id, userId, date)
                .orElseThrow(() -> new NotFoundException("Vote with Id=" + id + " not found"));

        if (time.isBefore(timeLimit)) {
            vote.setRestaurant(restaurantRepository.getReferenceById(restaurantId));
            voteRepository.save(vote);
        } else {
            throw new DataConflictException("Too late!");
        }
    }
}
