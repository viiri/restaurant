package ru.dvlife.x0r.restaurant.service;

import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dvlife.x0r.restaurant.error.NotFoundException;
import ru.dvlife.x0r.restaurant.model.Menu;
import ru.dvlife.x0r.restaurant.model.Restaurant;
import ru.dvlife.x0r.restaurant.repository.MenuRepository;
import ru.dvlife.x0r.restaurant.repository.RestaurantRepository;
import ru.dvlife.x0r.restaurant.to.MenuTo;

import java.time.Clock;
import java.time.LocalDate;

@Service
public class MenuService {
    @Autowired
    private Clock clock;
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private MenuRepository menuRepository;

    public Menu getExisted(int id) {
        return menuRepository.findByIdWithRestaurantAndDishes(id).orElseThrow(() -> new NotFoundException("Entity with id=" + id + " not found"));
    }

    @Transactional
    public Menu create(@NotNull MenuTo menuTo) {
        Restaurant restaurant = restaurantRepository.getReferenceById(menuTo.getRestaurantId());
        LocalDate date = menuTo.getDate() != null ? menuTo.getDate() : LocalDate.now(clock);
        return menuRepository.save(new Menu(null, restaurant, date, menuTo.getDishes()));
    }

    @Transactional
    public void update(@NotNull MenuTo menuTo, int menuId) {
        Restaurant restaurant = restaurantRepository.getReferenceById(menuTo.getRestaurantId());
        Menu menu = menuRepository.getExisted(menuId);

        menu.setRestaurant(restaurant);
        menu.getDishes().clear();
        menu.getDishes().addAll(menuTo.getDishes());

        menuRepository.save(menu);
    }
}
