package ru.dvlife.x0r.restaurant.web.restaurant;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import ru.dvlife.x0r.restaurant.model.Restaurant;
import ru.dvlife.x0r.restaurant.repository.RestaurantRepository;

import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class AbstractRestaurantController {
    protected final Logger log = getLogger(getClass());

    @Autowired
    protected RestaurantRepository repository;

    public Restaurant get(int id) {
        log.info("get {}", id);
        return repository.getExisted(id);
    }

    @Cacheable("restaurants")
    public List<Restaurant> getAll() {
        log.info("getAll");
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public List<Restaurant> getByName(String name) {
        log.info("getByName {}", name);
        return repository.findByNameContainsIgnoreCase(name);
    }
}
