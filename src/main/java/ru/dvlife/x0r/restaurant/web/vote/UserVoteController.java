package ru.dvlife.x0r.restaurant.web.vote;

import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.dvlife.x0r.restaurant.model.Vote;
import ru.dvlife.x0r.restaurant.repository.VoteRepository;
import ru.dvlife.x0r.restaurant.service.VoteService;
import ru.dvlife.x0r.restaurant.to.RestaurantTo;
import ru.dvlife.x0r.restaurant.to.VoteTo;
import ru.dvlife.x0r.restaurant.web.AuthUser;

import java.net.URI;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping(value = UserVoteController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserVoteController {
    protected final Logger log = getLogger(getClass());
    static final String REST_URL = "/api/votes";

    @Autowired
    protected VoteService service;
    @Autowired
    protected VoteRepository repository;
    @Autowired
    protected ModelMapper modelMapper;

    @GetMapping("current")
    public VoteTo getCurrent(@AuthenticationPrincipal AuthUser authUser) {
        return modelMapper.map(service.getCurrentByUser(authUser.id()), VoteTo.class);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VoteTo> create(@Valid @RequestBody RestaurantTo restaurantTo, @AuthenticationPrincipal AuthUser authUser) {
        Vote created = service.create(restaurantTo.getId(), authUser.id());
        URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(REST_URL + "/{id}")
                .buildAndExpand(created.getId()).toUri();

        return ResponseEntity.created(uriOfNewResource).body(modelMapper.map(created, VoteTo.class));
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Integer id, @Valid @RequestBody RestaurantTo restaurantTo, @AuthenticationPrincipal AuthUser authUser) {
        service.update(id, restaurantTo.getId(), authUser.id());
    }
}
