package ru.dvlife.x0r.restaurant.web.menu;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import ru.dvlife.x0r.restaurant.service.MenuService;
import ru.dvlife.x0r.restaurant.to.MenuTo;
import ru.dvlife.x0r.restaurant.util.MapperUtil;
import ru.dvlife.x0r.restaurant.model.Menu;
import ru.dvlife.x0r.restaurant.repository.MenuRepository;

import java.time.LocalDate;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class AbstractMenuController {
    protected final Logger log = getLogger(getClass());

    @Autowired
    protected MenuRepository repository;

    @Autowired
    protected MenuService service;

    @Autowired
    protected ModelMapper modelMapper;

    public MenuTo get(int id) {
        log.info("get {}", id);
        return modelMapper.map(service.getExisted(id), MenuTo.class);
    }

    @Cacheable("menus")
    public List<MenuTo> getByRestaurantId(int restaurantId) {
        log.info("getByRestaurantId {}", restaurantId);
        List<Menu> menus = repository.findByRestaurantIdWithRestaurantAndDishes(restaurantId, Sort.by(Sort.Direction.DESC, "date"));
        return MapperUtil.convertList(menus, menu -> modelMapper.map(menu, MenuTo.class));
    }

    @Cacheable("menus")
    public List<Menu> getByDate(LocalDate date) {
        log.info("getByDate {}", date);
        return repository.findByDateWithRestaurantAndDishes(date);
    }
}
