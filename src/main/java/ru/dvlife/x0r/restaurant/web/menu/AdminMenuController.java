package ru.dvlife.x0r.restaurant.web.menu;

import jakarta.validation.Valid;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.dvlife.x0r.restaurant.to.MenuTo;
import ru.dvlife.x0r.restaurant.util.validation.ValidationUtil;
import ru.dvlife.x0r.restaurant.model.Menu;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = AdminMenuController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminMenuController extends AbstractMenuController {
    static final String REST_URL = "/api/admin/menus";

    @Override
    @GetMapping("/{id}")
    public MenuTo get(@PathVariable int id) {
        return super.get(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        log.info("delete {}", id);
        repository.deleteExisted(id);
    }

    @Override
    @GetMapping("/by-restaurant")
    public List<MenuTo> getByRestaurantId(@RequestParam(name = "id") int restaurantId) {
        return super.getByRestaurantId(restaurantId);
    }

    @Override
    @GetMapping("/by-date")
    public List<Menu> getByDate(@RequestParam LocalDate date) {
        return super.getByDate(date);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(value = "menus", allEntries = true)
    public ResponseEntity<MenuTo> createWithLocation(@Valid @RequestBody MenuTo menuTo) {
        log.info("create {}", menuTo);
        ValidationUtil.checkNew(menuTo);
        Menu created = service.create(menuTo);
        URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(REST_URL + "/{id}")
                .buildAndExpand(created.getId()).toUri();
        return ResponseEntity.created(uriOfNewResource).body(modelMapper.map(created, MenuTo.class));
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @CacheEvict(value = "menus", allEntries = true)
    public void update(@Valid @RequestBody MenuTo menuTo, @PathVariable int id) {
        log.info("update {} with id={}", menuTo, id);
        ValidationUtil.assureIdConsistent(menuTo, id);
        service.update(menuTo, id);
    }
}
