package ru.dvlife.x0r.restaurant.web.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.dvlife.x0r.restaurant.model.Menu;
import ru.dvlife.x0r.restaurant.to.MenuTo;

import java.time.Clock;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = UserMenuController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserMenuController extends AbstractMenuController {
    static final String REST_URL = "/api/menus";

    @Autowired
    private Clock clock;

    @GetMapping("/current")
    public List<Menu> getCurrent() {
        return super.getByDate(LocalDate.now(clock));
    }

    @Override
    @GetMapping("/{id}")
    public MenuTo get(@PathVariable int id) {
        return super.get(id);
    }

    @Override
    @GetMapping("/by-restaurant")
    public List<MenuTo> getByRestaurantId(@RequestParam(name = "id") int restaurantId) {
        return super.getByRestaurantId(restaurantId);
    }

    @Override
    @GetMapping("/by-date")
    public List<Menu> getByDate(@RequestParam LocalDate date) {
        return super.getByDate(date);
    }
}
