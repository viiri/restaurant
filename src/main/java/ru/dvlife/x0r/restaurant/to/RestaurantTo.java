package ru.dvlife.x0r.restaurant.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantTo {
    @NotNull
    @JsonProperty("restaurant_id")
    Integer id;
}
