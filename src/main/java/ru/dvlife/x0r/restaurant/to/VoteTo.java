package ru.dvlife.x0r.restaurant.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class VoteTo extends BaseTo {
    @NotNull
    @JsonProperty("user")
    Integer userId;

    @NotNull
    @JsonProperty("restaurant")
    Integer restaurantId;

    @NotNull
    LocalDate created;

    public VoteTo(Integer id, Integer userId, Integer restaurantId, LocalDate created) {
        super(id);
        this.userId = userId;
        this.restaurantId = restaurantId;
        this.created = created;
    }
}
