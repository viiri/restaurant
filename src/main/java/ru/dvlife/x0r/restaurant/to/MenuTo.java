package ru.dvlife.x0r.restaurant.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import ru.dvlife.x0r.restaurant.model.Dish;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MenuTo extends BaseTo {
    @NotNull
    @JsonProperty("restaurant")
    Integer restaurantId;

    @NotNull
    LocalDate date;

    @NotNull
    List<Dish> dishes;

    public MenuTo(Integer id, Integer restaurantId, LocalDate date, List<Dish> dishes) {
        super(id);
        this.restaurantId = restaurantId;
        this.date = date;
        this.dishes = dishes;
    }
}
