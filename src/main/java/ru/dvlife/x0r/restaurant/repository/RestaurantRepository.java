package ru.dvlife.x0r.restaurant.repository;

import org.springframework.transaction.annotation.Transactional;
import ru.dvlife.x0r.restaurant.model.Restaurant;

import java.util.List;

@Transactional(readOnly = true)
public interface RestaurantRepository extends BaseRepository<Restaurant> {
    List<Restaurant> findByNameContainsIgnoreCase(String name);
}
