package ru.dvlife.x0r.restaurant.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.dvlife.x0r.restaurant.model.Menu;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Transactional(readOnly = true)
public interface MenuRepository extends BaseRepository<Menu> {
    @Query("SELECT m FROM Menu m LEFT JOIN FETCH m.restaurant " +
            "INNER JOIN FETCH m.dishes WHERE m.date = :date")
    List<Menu> findByDateWithRestaurantAndDishes(LocalDate date);

    @Query("SELECT m FROM Menu m LEFT JOIN FETCH m.restaurant " +
            "INNER JOIN FETCH m.dishes WHERE m.restaurant.id = :id")
    List<Menu> findByRestaurantIdWithRestaurantAndDishes(int id, Sort sort);

    @Query("SELECT m FROM Menu m LEFT JOIN FETCH m.restaurant " +
            "INNER JOIN FETCH m.dishes WHERE m.id = :id")
    Optional<Menu> findByIdWithRestaurantAndDishes(int id);
}
