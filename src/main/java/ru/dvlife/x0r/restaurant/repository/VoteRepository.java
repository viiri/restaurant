package ru.dvlife.x0r.restaurant.repository;

import org.springframework.transaction.annotation.Transactional;
import ru.dvlife.x0r.restaurant.model.Vote;

import java.time.LocalDate;
import java.util.Optional;

@Transactional(readOnly = true)
public interface VoteRepository extends BaseRepository<Vote> {
    Optional<Vote> findByUserIdAndCreated(int userId, LocalDate created);
    Optional<Vote> findByIdAndUserIdAndCreated(int id, int userId, LocalDate created);
}
