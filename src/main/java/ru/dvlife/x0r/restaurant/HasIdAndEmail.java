package ru.dvlife.x0r.restaurant;

public interface HasIdAndEmail extends HasId {
    String getEmail();
}